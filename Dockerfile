#################################
## Dockerfile Duplicati        ##
## SIMTIO                      ##
#################################

# Get SIMTIO template image
FROM simtio/base_image:0.5-buster

# Open web port
EXPOSE 8200

# Arguments
ARG DUPLICATI_RELEASE

# Add files
COPY entrypoint.sh /

# Install packages
RUN apt-get update \
  && apt-get install --no-install-recommends --no-install-suggests -qy jq unzip mono-devel \
  && if [ -z ${DUPLICATI_RELEASE+x} ]; then \
      DUPLICATI_RELEASE=$(curl -sX GET "https://api.github.com/repos/duplicati/duplicati/releases" \
      | jq -r 'first(.[] | select(.tag_name | contains("beta"))) | .tag_name'); \
     fi \
  && mkdir -p /app/duplicati \
  && duplicati_url=$(curl -s --retry 5 https://api.github.com/repos/duplicati/duplicati/releases/tags/"${DUPLICATI_RELEASE}" |jq -r '.assets[].browser_download_url' |grep zip |grep -v signatures) \
  && curl -o /tmp/duplicati.zip -L "${duplicati_url}" \
  && unzip -q /tmp/duplicati.zip -d /app/duplicati \
  && chmod +x ./entrypoint.sh \
  && apt-get remove --purge -qy unzip jq \
  && apt-get clean \
  && rm -rf /tmp/* /var/tmp/* /var/lib/apt/lists/* \
  ;

# Volumes
VOLUME /backups /config /source

# Entrypoint
ENTRYPOINT ["/entrypoint.sh"]
