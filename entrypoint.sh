#!/usr/bin/env bash

# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail

# Set path
PATH=/bin:/sbin:/usr/bin:/usr/sbin

############
## CONFIG ##
############
# Setup app user
PUID=${PUID:-911}
PGID=${PGID:-911}
groupmod -o -g "$PGID" duplicati
usermod -o -u "$PUID" duplicati
echo '
--------------------------------------------
  ___   ___   __  __   _____   ___    ___
 / __| |_ _| |  \/  | |_   _| |_ _|  / _ \
 \__ \  | |  | |\/| |   | |    | |  | (_) |
 |___/ |___| |_|  |_|   |_|   |___|  \___/

--------------------------------------------

Brought to you by SIMTIO
https://www.simtio.fr
--------------------------------------------
GID/UID
--------------------------------------------'
echo "
User uid:    $(id -u duplicati)
User gid:    $(id -g duplicati)
--------------------------------------------
"

# Fix rights
mkdir -v /config /source
chown -Rv duplicati:duplicati /app/duplicati
chown -v duplicati:duplicati /config /source

# Set trap to stop the script proprely when a docker stop is executed
trap : EXIT TERM KILL INT SIGKILL SIGTERM SIGQUIT

# Start Duplicati
CLI_ARGS=${CLI_ARGS:-}
cd /app/duplicati || exit
mono Duplicati.Server.exe \
	--webservice-interface=any --server-datafolder=/config --webservice-allowed-hostnames=* ${CLI_ARGS}
