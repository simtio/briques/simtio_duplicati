# SIMTIO - duplicati

This container contain Duplicati. Used as a backup solution.

## Starting the container & Configuring
```
docker run -d -v duplicati-data:/config -v duplicati-backups:/backups -v folders-to-backup:/source -p 8200:8200 --name=simtio-duplicati registry.gitlab.com/simtio/briques/simtio_duplicati
```

## Environment Parameters
|Name|Default|Use|
|DUPLICATI_RELEASE|Latest|Specify specific Duplicati release|
|PUID|911|App user uid - See below|
|PGID|911|App user gid - See below|
|CLI_ARGS|None|Optionally specify any [CLI variables](https://duplicati.readthedocs.io/en/latest/07-other-command-line-utilities/) you want to launch the app with|

When using volumes (-v flags) permissions issues can arise between the host OS and the container, we avoid this issue by allowing you to specify the user PUID and group PGID.

Ensure any volume directories on the host are owned by the same user you specify and any permissions issues will vanish like magic.

## Dev
```
# Gitlab
docker build -t registry.gitlab.com/simtio/briques/simtio_duplicati:latest -t registry.gitlab.com/simtio/briques/simtio_duplicati:0.x .
docker push registry.gitlab.com/simtio/briques/simtio_duplicati && docker push registry.gitlab.com/simtio/briques/simtio_duplicati:0.x

# Dockerhub
docker build -t simtio/duplicati:latest -t simtio/duplicati:0.x .
docker push simtio/duplicati:latest && docker push simtio/duplicati:0.x

docker buildx build -t simtio/duplicati:latest -t simtio/duplicati:0.x --platform=linux/aarch64,linux/amd64,linux/arm . --push

docker run --rm -p 8200:8200 --name=simtio-duplicati simtio/duplicati
```

## Sources
- https://github.com/linuxserver/docker-duplicati
- https://github.com/duplicati/duplicati/wiki/Headless-installation-on-Debian-or-Ubuntu
